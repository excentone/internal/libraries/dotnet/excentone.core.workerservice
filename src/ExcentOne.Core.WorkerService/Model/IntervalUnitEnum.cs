﻿namespace ExcentOne.Core.WorkerService.Model
{
    public enum IntervalUnitEnum
    {

        Second = 0,
        Minute = 1,
        Hour = 2,
        Day = 3

    }
}
